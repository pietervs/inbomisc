#models.r

overdispersion <- function(x, ...){
    UseMethod("overdispersion")
}

overdispersion.glm <- function(x, ...){
    sum(residuals(x, type = "pearson")^2) / (length(residuals(x)) - length(coef(x)))
}