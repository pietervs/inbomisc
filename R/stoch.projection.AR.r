#' Stochastische projectie uitbreiding op popbio functie stoch.projection
#' 
#' Eigenlijk is de functie zelf deterministisch, je kan gewoon een selectievector (die je wel random kan laten genereren) doorgeven en op basis hiervan wordt de functie uitgevoerd
#' @param matrices Dit bevat een lijst met verschillende populatiematrices. Deze zijn verplicht vierkant en allemaal bestaande uit dezelfde dimensies
#' @param selection Een vector die bepaalt in welke volgorde de matrices geselecteerd worden. De lengte van deze vector bepaalt automatisch hoeveel tijdsstappen de functie zal doorlopen, namelijk evenveel als de selection vector lang is.
#' @param startvector de vector met de startpopulatie in jaar 0. Deze moet even veel elementen bevatten als het aantal rijen en kolommen van de matrices
#' @return Een lijst met volgende elementen:
#' \itemize{
#'      \item{"Matrices"}{De lijst met keuzematrices}
#'      \item{"Selection"}{De selectievolgorde van de matrices}
#'      \item{"AgeDist"}{De leeftijdsdistributie op elk tijdstip}
#'      \item{"pop.sizes"}{De totale populatie op elk tijdstip}
#'      \item{"pop.changes"}{De verandering in populatie op elk tijdstip}
#'      \item{"stage.vectors"}{De effectieve aantallen per klasse op elk tijdstip}
#'  }
pop.projection.AR <-  
    function(matrices, selection, startvector){
        selection <- as.character(selection)
        nyears <- length(selection)
        nam <- names(matrices)
        if (!all(selection %in% nam)) {
            stop("Selection does not correspond with names of matrix list")
        }
        n <- length(matrices)
        testvar <- sapply(matrices, dim)
        s <- unique(as.vector(testvar)) #stage classes
        if (length(s) >  1){
            stop("All matrices should be square and have the same dimensions")
        }
        len <- length(startvector)
        if (len != s) {
            stop("The startvector should be as long as there are columns in the matrices")
        }
        resultMatrix <- matrix(ncol = len, nrow = nyears+1, data = NA)
        rnam <- as.character(c(0, 1:nyears))
        rownames(resultMatrix) <- rnam
        colnames(resultMatrix) <- names(startvector)
        resultMatrix["0",] <- startvector
        resultMatrix
        for (i in 1:(nyears)) {
            prevrow <- rnam[i]
            row <- rnam[i+1]
            mat <- matrices[[selection[i]]]
            resultMatrix[i+1, ] <- mat %*% resultMatrix[i,] #resultmMatrix[i,] = vector = matrix met 1 kolom       
        }
        agedist <- apply(resultMatrix, 1, function(dat) dat / sum(dat))
        pop.sizes <-  rowSums(resultMatrix)
        pop.changes <- data.frame(Year = 1:nyears,
                                  Change = tail(rowSums(resultMatrix),-1) / head(rowSums(resultMatrix),-1),
                                  MatrixName = selection)
        #--not used---lambda <- tapply(pop.changes$Change, pop.changes["MatrixName"], function(x) x[length(x)] ) #Get the last lambda for each matrix
        
        list(Matrices = matrices,
             Selection = selection,
             AgeDist = agedist,
             pop.sizes = pop.sizes,
             pop.changes = pop.changes,
             #--not used--- lambda = lambda,
             stage.vectors = t(resultMatrix))
    }

