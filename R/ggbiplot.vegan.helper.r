
.get.df.u <- function(x, data, choices, scaling){
    df.u <- as.data.frame(scores(x, display = "sites", scaling = scaling, choices=choices))
    if(all(substring(rownames(df.u),1,3) == "row")) 
        rownames(df.u) <- substring(rownames(df.u), 4, nchar(rownames(df.u)))

    u.axis.labs <- colnames(df.u)[choices]

    df.u <- merge(df.u, data, by = "row.names", sort = FALSE)[,-1]
    if (scaling == 2) {
        u.axis.labs = paste("standardized", u.axis.labs)
    }

    ### Explained Variance
    # Append the proportion of explained variance to the axis labels
    # de eigenwaarden van de unconstrained assen zitten in pcobj$CA$eig
    # de eigenwaarden van de constrained assen zitten in pcobj$CCA$eig
    u.axis.labs <- 
        paste(u.axis.labs, 
              sprintf('(%0.1f%% explained var.)', 
                      100 * c(x$CCA$eig, x$CA$eig)[choices] / sum(c(x$CCA$eig, x$CA$eig))))
    
    attr(df.u, "axis.labels") <- u.axis.labs
    df.u
}

###


.get.df.v <- function(x, choices, scaling, varname.abbrev = FALSE, varname.adjust = 1.1){
    df.v <- as.data.frame(scores(x, display = "species", scaling = scaling, choices=choices))
    if(varname.abbrev) {
        df.v$varname <- abbreviate(rownames(df.v))
    } else {
        df.v$varname <- rownames(df.v)
    }
    ### Variables for text label placement
    df.v$angle <- (180/pi) * atan(df.v[,2] / df.v[,1])
    df.v$hjust <- (1 - varname.adjust * sign(df.v[,1])) / 2
    
    df.v
}

###

.get.df.lc <- function(x, data, choices, scaling){
    lc <- scores(x, display = "lc", scaling = scaling, choices=choices)
    q <- ncol(x$CCA$v)
    if (!is.null(q)){
        df.lc <- as.data.frame(lc)
        df.lc <- merge(df.lc, data, by = "row.names", sort = FALSE)[,-1]
    } else {
        df.lc <- NULL
    }
    df.lc
}

###

.get.df.bp <- function(x, choices, scaling, varname.adjust = 0.05) {
    q <- ncol(x$CCA$v)
    bp <- scores(x, display = "bp", scaling = scaling, choices=choices)
    cn <- scores(x, display = "cn", scaling = scaling, choices=choices)
    if (!is.null(q)){
        if(!is.na(cn)[1]) {
            bipnam <- rownames(bp)
            cntnam <- rownames(cn)
            bp <- bp[!(bipnam %in% cntnam), , drop = FALSE]
            df.bp <- as.data.frame(bp)
            if (nrow(bp) == 0) df.bp <- NULL
                
        } else {
            df.bp <- as.data.frame(bp)
        }
    } else {
        df.bp <- NULL
    }
    if(!is.null(df.bp)){
        df.bp$bpname <- rownames(bp)
        df.bp$angle <- (180/pi) * atan(df.bp[,2] / df.bp[,1])
        df.bp$hjust = (1 - varname.adjust * sign(df.bp[,1])) / 2
    }
    print(df.bp)
    df.bp
}




###

.get.df.cn <- function(x, choices, scaling) {
    q <- ncol(x$CCA$v)
    cn <- scores(x, display = "cn", scaling = scaling, choices=choices)
    levlist <- x$terminfo$xlev


    if (!is.null(q)) {
        if (!is.na(cn)[1]) {
            df.cn <- as.data.frame(cn)
        } else {
            df.cn <- NULL 
        }
    } else {
        df.cn <- NULL 
    }
    
    if(!is.null(q) & !is.na(cn)[1]){
        # centroid names
        
        levinterpret <- 
            do.call("rbind",
                    lapply(names(levlist), levlist = levlist, 
                        FUN= function(levs, levlist){ 
                        cbind(.Level = levlist[[levs]], 
                              .FullName = paste(levs, levlist[[levs]], sep = ""))
                     }))
        df.cn$.cnname <- rownames(cn)
        df.cn <- merge(df.cn, levinterpret, by.x = ".cnname", by.y = ".FullName", sort = FALSE)
        df.cn <- df.cn[c(2,3,1,4)] #door de merge wordt de merge kolom eerst gezet, en we willen dat dit de scores zijn
    }
    df.cn
}
    
###############################################

.ggbiplot.vegan_descdata <- function(x, data = NULL, groupsvarname = NULL, ellgroupsvarname = NULL, labelsvarname = NULL, ...){
    n_obs <- nrow(scores(x)$sites)
    if (!is.null(data)){
        if (length(groupsvarname) == 1) {
            data$.groups <- data[,groupsvarname]
            attr(data, "groups") <- groupsvarname
        } else {
            data$.groups <- rep(1, n_obs)
            attr(data, "groups") <- NA
        }
        if (length(ellgroupsvarname) == 1) {
            data$.ellgroups <- data[,ellgroupsvarname] 
            attr(data, "ellgroups") <- ellgroupsvarname
        } else {
            data$.ellgroups <- 1
            attr(data, "ellgroups") <- NA
        }
        if (length(labelsvarname) == 1) {
            data$.labels <- data[, labelsvarname]  
            attr(data, "labels") <- labelsvarname
        } else {
            data$.labels <- row.names(data)
            attr(data, "labels") <- "row.names"
        }
        
    } else { #if data is NULL}
        data <- data.frame(.groups = rep(1, n_obs), .ellgroups = rep(1, n_obs), .labels = rownames(x$CA$u), stringsAsFactors = FALSE)
        if(all(substring(data$.labels,1,3) == "row")) 
            data$.labels <- substring(data$.labels, 4, nchar(data$.labels))
        attr(data, "groups") <- NA
        attr(data, "ellgroups") <- NA
        attr(data, "labels") <- NA
        row.names(data) <- data$.labels
    } 
    data$.labels <- as.character(data$.labels)

    data 
}

########################################################################

 .fun.ellipse <- function(x, g, ellipse.prob, circle) {
     if(nrow(x) >= 2) {
         colnames(x)[1] <- as.character(g$mapping$x)
         colnames(x)[2] <- as.character(g$mapping$y)
         sigma <- var(x[,1:2])
         sigma <- round(sigma, 7) #gewoon om af te dwingen dat de matrix symmetrisch is
         mu <- c(mean(x[,1]), mean(x[,2]))
         print(mu)
         ed <- sqrt(qchisq(ellipse.prob, df = 2))
         data.frame(sweep(circle %*% chol(sigma) * ed, 2, mu, FUN = '+'), .ellgroups = x$.ellgroups[1])
     } else {
         NULL
     }
     
 }       


.fun.ellgroups <-  function(x, g, ellipse.prob, circle) {
    cat("in .fun.ellgroups\n")
    x <- as.data.frame(x)
    if(nrow(x) >= 2) {
        colnames(x)[1] <- as.character(g$mapping$x)
        colnames(x)[2] <- as.character(g$mapping$y)
        sigma <- var(x[,1:2])
        sigma <- round(sigma, 7) #gewoon om af te dwingen dat de matrix symmetrisch is
        cat("names x\n"); print(names(x))
        cat("head_x\n");print(head(x[,1:2]))
        mu <- c(mean(x[,1]), mean(x[,2]))
        cat("mu of ellipse\n")
        print(mu)
        ed <- sqrt(qchisq(ellipse.prob, df = 2))
        data.frame(sweep(circle %*% chol(sigma) * ed, 2, mu, FUN = '+'), .ellgroups = x$.ellgroups[1])
    } else {
        NULL
    }
}


######################################################################
###################################################################### 


