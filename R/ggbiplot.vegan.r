#' GGplot-style figure for biplots and triplots for methods of the vegan package
#'
#' @param x vegan object of class rda, cca, capscale
#' @param ... other arguments for the specific functions
#' @param data dataset with descriptive columns, linked by rownames with x. If no data is specified then R will make a default descriptive dataset.
#' @param choices Vector of 2 elements: which scores do you want to present in your plot?
#' @param scaling documentation vegan::scores  covariance biplot (scaling = 2), form biplot (scaling = 1). When scale = 1, the inner product between the variables approximates the covariance and the distance between the points approximates the Mahalanobis distance.
#' @param varname.abbrev logical Do you want to abbreviate the variable names?
#' @param varname.adjust When the arrow labels in the biplot are overlapping the arrows, specifying another value of varname.adjus can take care of this. The value needed depends on the length of the labels and de range in the plot.

#' @return GGplot object containint the information to show a biplot or triplot
#' @rdname ggbiplot.vegan
#' @export
#' @examples
#' \dontrun{
#' require(vegan)
#' require(ggplot2)
#' data(iris)
#' iris$Species2 <- iris$Species
#' levels(iris$Species2) <- list(Sgroup = "setosa", Vgroup = c("versicolor", "virginica"))
#' rdaobj <- rda(iris[,1:4] ~ 1)
#' rdaobj2 <- rda(iris[,1:4]  ~ Species, data = iris)
#' capobj2 <- capscale(iris[,1:4] ~ Species, data = iris)
#'
#'ggbiplot.vegan(rdaobj, show.labels = FALSE) 
#'  
#'ggbiplot.vegan(rdaobj, data = iris, groupsvarname = "Species", ellgroupsvarname = "Species2") 
#'ggbiplot.vegan(rdaobj, data = iris, groupsvarname = "Species", ellgroupsvarname = "Species2", labelsvarname = "Sepal.Width", show.labels = FALSE, site.scores = FALSE, var.as.arrows = FALSE)
#'
#' 
#'  
#'data(varechem)
#'data(varespec)
#'myvarechemdesc <- varechem["pH"]
#'myvarechemdesc$pHclass <- factor(floor(myvarechemdesc$pH))
#'myvarechemdesc$Loco <- factor(sample(c("a","b","d"), nrow(myvarechemdesc), replace = TRUE))
#'vare.cca <- cca(varespec  ~ N + P + K + Ca, data = varechem)
#'plot(vare.cca)
#'ggbiplot.vegan(vare.cca, var.as.arrows = FALSE) #is niet zoals gewenst
#'
#'
#'
#'data(dune)
#'data(dune.env)
#'mod <- cca(dune ~ A1 + Moisture + Management, dune.env)
#'ggbiplot.vegan(mod, data = dune.env, show.labels = FALSE, var.as.arrows = TRUE, var.axes = TRUE, groupsvarname = "Management")
#'}
#'
#'
#'library(vegan)
#'library(ggplot2)
#'data(varechem)
#'data(varespec)
#'varechem$Humdepth <- factor(round(varechem$Humdepth))
#'varechem$labels.cn <- paste("HD:",as.numeric(varechem$Humdepth),sep="")
#'varespec.log <- log(varespec + 1)
#'mod.cap <- capscale(varespec.log ~   N , data=varechem,distance="bray", add=TRUE)
#'ggbiplot.vegan(mod.cap, choices = 1:2, data = varechem, labelsvarname = "labels.cn", scaling = 2, var.axes = TRUE, var.as.arrows = FALSE,  env.axes = c("cn", "bp"), circle = TRUE)
ggbiplot.vegan <- function(x, ...) {
    UseMethod("ggbiplot.vegan")
}

######################################################################

#' @export
#' @method ggbiplot.vegan rda
#' @rdname  ggbiplot.vegan
ggbiplot.vegan.rda <- function(x, data=NULL, choices = 1:2, scaling = 2, varname.abbrev = FALSE, varname.adjust = 1.1, var.as.arrows = TRUE, ...)    
{ 
    stopifnot(length(choices) == 2)
    data <- .ggbiplot.vegan_descdata(x, data, ...)
    df.u <- .get.df.u(x, data, choices, scaling)
    df.v <- .get.df.v(x, choices, scaling, varname.abbrev, varname.adjust)
    df.lc <- .get.df.lc(x, data, choices, scaling)
    df.bp <- .get.df.bp(x, choices, scaling, varname.adjust)    
    df.cn <- .get.df.cn(x, choices, scaling)    
    ggbiplot.vegan.default(df.u=df.u, df.v=df.v, df.cn=df.cn, df.lc=df.lc, df.bp=df.bp, axis.labels = attr(df.u,"axis.labels"), var.as.arrows = var.as.arrows, ...)
    
}

######################################################################

#' @export
#' @method ggbiplot.vegan cca
#' @rdname  ggbiplot.vegan
ggbiplot.vegan.cca <- function(x, data=NULL, choices = 1:2, scaling = 2, varname.abbrev = FALSE, varname.adjust = 1.1, var.as.arrows = FALSE, ...)    
{ 
    stopifnot(length(choices) == 2)
    data <- .ggbiplot.vegan_descdata(x, data, ...)
    df.u <- .get.df.u(x, data, choices, scaling)
    df.v <- .get.df.v(x, choices, scaling, varname.abbrev, varname.adjust)
    df.lc <- .get.df.lc(x, data, choices, scaling)
    df.bp <- .get.df.bp(x, choices, scaling, varname.adjust)     
    df.cn <- .get.df.cn(x, choices, scaling)    
    ggbiplot.vegan.default(df.u=df.u, df.v=df.v, df.cn=df.cn, df.lc=df.lc, df.bp=df.bp, axis.labels = attr(df.u,"axis.labels"), var.as.arrows = var.as.arrows, ...)
}

######################################################################

#' @export
#' @method ggbiplot.vegan capscale
#' @rdname  ggbiplot.vegan
ggbiplot.vegan.capscale <- function(x, data=NULL, choices = 1:2, scaling = 2, varname.abbrev = FALSE, varname.adjust = 1.1, var.as.arrows = FALSE,  ...)    
{ 
    stopifnot(length(choices) == 2)
    data <- .ggbiplot.vegan_descdata(x, data,  ...)
    df.u <- .get.df.u(x, data, choices, scaling)
    df.v <- .get.df.v(x, choices, scaling, varname.abbrev, varname.adjust)
    df.lc <- .get.df.lc(x, data, choices, scaling)
    df.bp <- .get.df.bp(x, choices, scaling, varname.adjust)    
    df.cn <- .get.df.cn(x, choices, scaling)    
    ggbiplot.vegan.default(df.u=df.u, df.v=df.v, df.cn=df.cn, df.lc=df.lc, df.bp=df.bp, axis.labels = attr(df.u,"axis.labels"), var.as.arrows = var.as.arrows, ...)
}


######################################################################

#' @param df.u 2-column dataset containing the scores, with axis.labels attributes
#' @param df.v 5-column dataset with 2 species scores components, varname to store the label of the arrow, angle to specify the angle of the label and hjust to specify the horizontal adjustment of the label
#' @param df.cn 4-column dataset with 2 centroid scores, .cnname, en .Level. Analog to df.bp, but for categorical data that are presented as centroids. .cnname contains the full variablename with level, .Level only the level to burden the plot less.
#' @param df.lc dataset containing 2 linear contstraint scores and all columns of the descriptive data, which are linked by row.names to the vegan object
#' @param df.bp 5-column dataset with 2 bp scores, bpname, angle, hjust, with bpname the label to print and angle and hjust analog to df.v. df.bp contains the data to plot arrows of explorative variables not contained in the model
#' @param site.scores logical flag controlling whether the site.scores (row.scores) are plot
#' @param var.axes logical flag controlling whether the species.scores (column.scores) are plot
#' @param lc.scores logical flag controlling whether the linear constraints are plot
#' @param draw.ellipse logical flag controlling whether a density ellipse is shown for each unique value of ellgroupsvarneme with a probability corresponding to ellipse.prob
#' @param groupsvarname variable name (existing in data) on which the sites will be colored
#' @param ellgroupsvarname variable name (existing in data) on which the ellipses will be separated
#' @param ellipse.prob the probability density of the ellipse (should be between 0 and 1, 0.68 corresponds to 1 normal standard deviation, 0.95 to 2 normal standard deviations)
#' @param labelsvarname the variable name (existing in data) with the labels for the site.scores
#' @param labels.size controls the size of the site.score labels
#' @param show.labels logical flag controlling of labels are plot, or just generic points
#' @param alpha.r ==> NOG AANVULLEN 
#' @param alpha.c  ==> NOG AANVULLEN
#' @param var.as.arrows logical flag controlling if var.axes should be arrows or just points with labels
#' @param varname.size controls the size of the species.scores labels
#' @param circle logical flag controlling if a correlation circle must be drawn. Only meaningful when scaling = 2
#' @param circle.prob The probability (between 0 and 1) how much of the total variation corresponds to the circle, if 1 then the circle shows how much of the variability of a variable is explained in the 2 selected components
#' @param env.axes ==> nog aanvullen
#' @param verbose not used variable for the moment to allow the function to show its progress
#' @param base.colours a vector of at least 5 colors, that will serve as base colors for the plot content. 
#' @param axis.labels a vector of size 2 allowing custom axis names
#' @param show.only.centroid.level  ==> NOG AANVULLEN
#' @export
#' @method ggbiplot.vegan default
#' @rdname  ggbiplot.vegan
ggbiplot.vegan.default <- 
    function(x, df.v, df.lc, df.bp, df.cn,
             site.scores = TRUE, var.axes = TRUE, lc.scores = TRUE,  draw.ellipse = !is.null(ellgroupsvarname),
             groupsvarname = NULL,
             ellgroupsvarname = NULL, ellipse.prob = 0.68, 
             labelsvarname = NULL, labels.size = 3, show.labels = TRUE, alpha.r = 1, alpha.c = 1, 
             var.as.arrows = TRUE, varname.size = 3, varname.adjust = 1.5, varname.abbrev = FALSE,
             circle = FALSE, circle.prob = 0.68, 
             env.axes = c("bp", "cn"), verbose = FALSE,
             base.colours = c("#989868","#688599","#CC3D3D","#CACC41", "#B66546"), #first 5 INBOtheme colours
             axis.labels = c("Comp 1", "Comp 2"), show.only.centroid.level = TRUE)
    {

        df.u <- x

        ###load libraries
        require(ggplot2); require(dplyr); require(scales); require(grid); require(vegan)   
        
        ###Match arguments
        env.axes <- match.arg(arg = env.axes, several.ok = TRUE)
        #row.scores <- match.arg(arg = row.scores, several.ok = TRUE)
        
        ###Initizalize variables
        df.u <- x
        n <- nobs(df.u)
        p <- ncol(df.v) 
        v.scale <- rowSums(df.v[,1:2])^2 
        r <- sqrt(max(v.scale)) #radius of the correlation circle
        
        mybreaks <- pretty(c(df.u$xvar, df.u$yvar), n = 5, eps.correct =  1)
        u.axis.labs <- axis.labels
        
        
        ### >>> Effective Plot
        ###=========================
        
        ### Sites plot
        
        g <- ggplot(data = df.u, aes_string(x = names(df.u)[1], y = names(df.u)[2])) + 
            xlab(u.axis.labs[1]) + ylab(u.axis.labs[2]) + coord_equal() +
            scale_x_continuous(breaks = mybreaks) + 
            scale_y_continuous(breaks = mybreaks)
        
        if(site.scores == TRUE) {
            if (show.labels){
                if(length(unique(df.u$.groups)) > 1) {
                    g <- g + geom_text(aes_string(label = ".labels", color = ".groups"), size = labels.size, alpha = alpha.r)
                } else {
                    g <- g + geom_text(aes_string(label = ".labels"), size = labels.size, alpha = alpha.r) 
                }        
            } else{
                if(length(unique(df.u$.groups)) > 1) {
                    g <- g + geom_point(aes_string(color = ".groups"), size = labels.size, alpha = alpha.r)
                } else {
                    g <- g + geom_point(size = labels.size, alpha = alpha.r) 
                }  
            }
        } 
        
        
        
        ### Species plot
        
        if (var.axes && var.as.arrows) {
            if(circle) {
                theta <- c(seq(-pi, pi, length = 50), seq(pi, -pi, length = 50))
                circle <- data.frame(xvar = r * cos(theta), yvar = r * sin(theta))
                g <- g + geom_path(aes_string(x = "xvar", y = "yvar"), data = circle, color = base.colours[2], size = 1/2, alpha = 1/3)
            }
            g <- g +
                geom_segment(data = df.v, aes_string(x = 0, y = 0, xend = names(df.v)[1], yend = names(df.v)[2]),
                             arrow = arrow(length = unit(1/2, 'picas')), 
                             color = base.colours[2], alpha = alpha.c)
            
            g <- g + geom_text(data = df.v, 
                               aes_string(label = "varname", x = names(df.v)[1], y = names(df.v)[2], angle = "angle", hjust = "hjust"), 
                               color = base.colours[2], size = varname.size, alpha = alpha.c)
            
        }
        
        if (var.axes && !var.as.arrows) {
            g <- g +
                geom_point(data = df.v, aes_string(x = names(df.v)[1], y = names(df.v)[2]),
                           shape = 2, color = base.colours[2],alpha = alpha.c)
            g <- g + geom_text(data = df.v, aes_string(label = "varname", x = names(df.v)[1], y = names(df.v)[2], 
                                                       hjust = 0, vjust = 0), 
                               color = base.colours[2], size = varname.size, alpha = alpha.c)
            
        } 
        
        ### Correlatiecirkel
        
        if(circle) {
            theta <- c(seq(-pi, pi, length = 50), seq(pi, -pi, length = 50))
            circle <- data.frame(xvar = r * cos(theta), yvar = r * sin(theta))
            g <- g + geom_path(data = circle, aes_string(x = "xvar", y = "yvar"), color = base.colours[2], 
                               size = 1/2, alpha = 1/3)
        }
        
        
        ### lc plot
        
        if (!is.null(df.lc) && lc.scores == TRUE){
            if (show.labels){
                if(length(unique(df.lc$.groups)) > 1) {
                    g <- g + geom_text(data = df.lc, aes_string(x= names(df.lc)[1], y = names(df.lc)[2] ,label = ".labels", color = ".groups"), size = labels.size, alpha = alpha.r)
                } else {
                    g <- g + geom_text(data = df.lc, aes_string(x= names(df.lc)[1], y = names(df.lc)[2] ,label = ".labels"), size = labels.size, alpha = alpha.r) 
                }        
            } else{
                if(length(unique(df.u$.groups)) > 1) {
                    g <- g + geom_point(data = df.lc, aes_string(x= names(df.lc)[1], y = names(df.lc)[2] ,color = ".groups"), size = labels.size, alpha = alpha.r, shape = 2)
                } else {
                    g <- g + geom_point(data = df.lc, aes_string(x= names(df.lc)[1], y = names(df.lc)[2]), size = labels.size, alpha = alpha.r, shape = 2) 
                }  
            }        
            
        } else {
            #do not plot 
        }

        ### ellipse
        
        if (length(unique(df.u$.ellgroups)) > 1){
            theta <- c(seq(-pi, pi, length = 50), seq(pi, -pi, length = 50))

            circle <- cbind(cos(theta), sin(theta)) 
            df.u.gr <- group_by(df.u, .ellgroups)
            ell <- do(df.u.gr, .fun.ellipse(., g, ellipse.prob, circle)) 
            if(!is.null(ell)){
                g <- g + geom_path(data = ell, aes_string(x = colnames(ell)[1], y = colnames(ell)[2], color = ".ellgroups"))              }
        }
        
        
                
        ### Environmental variables
     
        if("bp" %in% env.axes && !is.null(df.bp)) { #!is.null(q) weggelaten
            g <- g + 
                geom_segment(data = df.bp,
                             aes_string(x = 0, y = 0, xend = colnames(df.bp)[1], yend = colnames(df.bp)[2]),
                             arrow = arrow(length = unit(1/2, 'picas')), 
                             color = base.colours[5]) + 
                geom_text(data = df.bp, 
                          aes_string(label = "bpname", x = colnames(df.bp)[1], y = colnames(df.bp)[2], 
                                     angle = "angle", hjust = "hjust"), 
                          color = base.colours[5], size = varname.size)
        }
        
        if("cn" %in% env.axes && !is.null(df.cn)) { #weggelaten:  !is.null(q) 
            g <- g + 
                geom_point(data = df.cn,
                           aes_string(x = colnames(df.bp)[1], y = colnames(df.bp)[2]),
                           shape = 3, color = base.colours[5]) +
                geom_text(data = df.cn,
                          aes_string(label = ifelse(show.only.centroid.level == TRUE, ".Level", ".cnname"),
                                     x = colnames(df.bp)[1],
                                     y = colnames(df.bp)[2]),
                          color = base.colours[5], size = varname.size, hjust = -0.20)
        }
        
        
        ### Legend
        g <- g + scale_colour_discrete(name = groupsvarname)
        g
    }


#########
